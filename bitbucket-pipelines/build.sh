#!/bin/sh

export DATA_TAG=$BITBUCKET_COMMIT
docker login --username $DOCKER_REGISTRY_USERNAME --password $DOCKER_REGISTRY_PASSWORD
docker build . -f docker/app/Dockerfile -t "registry.tshdev.io/webinar/prisoners/app:$DATA_TAG"
docker push registry.tshdev.io/webinar/prisoners/app:$DATA_TAG
docker logout