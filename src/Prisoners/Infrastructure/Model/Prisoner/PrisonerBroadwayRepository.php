<?php declare(strict_types=1);

namespace Prisoners\Infrastructure\Model\Prisoner;

use Broadway\EventHandling\EventBus;
use Broadway\EventSourcing\AggregateFactory\NamedConstructorAggregateFactory;
use Broadway\EventSourcing\EventSourcingRepository;
use Broadway\EventStore\EventStore;
use Prisoners\Domain\Model\Prisoner\Prisoner;
use Prisoners\Domain\Model\Prisoner\PrisonerId;
use Prisoners\Domain\Model\Prisoner\PrisonerRepository;

final class PrisonerBroadwayRepository implements PrisonerRepository
{
    /**
     * @var EventSourcingRepository
     */
    private $eventSourcingRepository;

    public function __construct(
        EventStore $eventStore,
        EventBus $eventBus,
        array $eventStreamDecorators = []
    ) {
        $this->eventSourcingRepository = new EventSourcingRepository(
            $eventStore,
            $eventBus,
            Prisoner::class,
            new NamedConstructorAggregateFactory(),
            $eventStreamDecorators
        );
    }

    public function get(PrisonerId $prisonerId): ?Prisoner
    {
        return $this->eventSourcingRepository->load($prisonerId->get());
    }

    public function save(Prisoner $prisoner): void
    {
        $this->eventSourcingRepository->save($prisoner);
    }
}
