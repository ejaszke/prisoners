<?php declare(strict_types=1);

namespace Prisoners\Infrastructure\Command;

use Broadway\CommandHandling\CommandBus;
use Prisoners\Application\Penitentiary\Command\ReleasePrisonerCommand;
use Prisoners\Domain\Model\Penitentiary\PenitentiaryId;
use Prisoners\Domain\Model\Prisoner\PrisonerId;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class ReleasePrisonerCliCommand extends Command
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        parent::__construct();
        $this->commandBus = $commandBus;
    }

    protected function configure(): void
    {
        $this->setName('prisoners:release');
        $this->addArgument('penitentiaryId', InputArgument::REQUIRED, 'Penitentiary id');
        $this->addArgument('prisonerId', InputArgument::REQUIRED, 'Prisoner id');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $penitentiaryId = $input->getArgument('penitentiaryId');
        $prisonerId = $input->getArgument('prisonerId');

        $assignPrisoner = new ReleasePrisonerCommand(new PenitentiaryId($penitentiaryId), new PrisonerId($prisonerId));

        $this->commandBus->dispatch($assignPrisoner);

        $output->writeln('<info>Prisoner released</info>');
    }
}
