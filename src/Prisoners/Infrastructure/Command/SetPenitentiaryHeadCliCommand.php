<?php declare(strict_types=1);

namespace Prisoners\Infrastructure\Command;

use Broadway\CommandHandling\CommandBus;
use Prisoners\Application\Penitentiary\Command\SetHeadCommand;
use Prisoners\Domain\Model\Penitentiary\PenitentiaryId;
use Prisoners\Domain\Model\User\UserId;
use Prisoners\Domain\ReadModel\User\UsersRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class SetPenitentiaryHeadCliCommand extends Command
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var UsersRepository
     */
    private $usersRepository;

    public function __construct(CommandBus $commandBus, UsersRepository $usersRepository)
    {
        parent::__construct();
        $this->commandBus = $commandBus;
        $this->usersRepository = $usersRepository;
    }

    protected function configure(): void
    {
        $this->setName('prisoners:penitentiary:set-head');
        $this->addArgument('penitentiaryId', InputArgument::REQUIRED, 'Penitentiary id');
        $this->addArgument('username', InputArgument::REQUIRED, 'User name');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $penitentiaryId = $input->getArgument('penitentiaryId');
        $username = $input->getArgument('username');

        $user = $this->usersRepository->findByUsername($username);

        $assignPrisoner = new SetHeadCommand(new PenitentiaryId($penitentiaryId), new UserId($user->getId()));

        $this->commandBus->dispatch($assignPrisoner);

        $output->writeln('<info>Head successfully assigned</info>');
    }
}
