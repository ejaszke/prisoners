<?php declare(strict_types=1);

namespace Prisoners\Domain\ReadModel\Penitentiary;

use Broadway\ReadModel\SerializableReadModel;

final class Penitentiary implements SerializableReadModel
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $headId;

    public function __construct(string $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setHeadId(string $headId): void
    {
        $this->headId = $headId;
    }

    public function getHeadId(): string
    {
        return $this->headId;
    }

    public static function deserialize(array $data): self
    {
        $penitentiary = new self($data['id'], $data['name']);
        if ($data['head_id']) {
            $penitentiary->setHeadId($data['head_id']);
        }

        return $penitentiary;
    }

    public function serialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'head_id' => $this->headId,
        ];
    }
}
