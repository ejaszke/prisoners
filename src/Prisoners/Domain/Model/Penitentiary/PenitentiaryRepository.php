<?php declare(strict_types=1);

namespace Prisoners\Domain\Model\Penitentiary;

interface PenitentiaryRepository
{
    public function get(PenitentiaryId $penitentiaryId): ?Penitentiary;

    public function save(Penitentiary $penitentiary): void;
}
