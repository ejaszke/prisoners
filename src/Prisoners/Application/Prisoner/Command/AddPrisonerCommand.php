<?php declare(strict_types=1);

namespace Prisoners\Application\Prisoner\Command;

use Prisoners\Domain\Model\Penitentiary\PenitentiaryId;
use Prisoners\Domain\Model\Prisoner\PrisonerId;

final class AddPrisonerCommand
{
    /**
     * @var PrisonerId
     */
    public $prisonerId;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var string
     */
    public $crime;

    /**
     * @var string
     */
    public $crimeCategory;

    /**
     * @var PenitentiaryId
     */
    public $penitentiaryId;

    /**
     * @var \DateTimeInterface
     */
    public $startDate;

    /**
     * @var \DateTimeInterface
     */
    public $endDate;

    /**
     * @var string
     */
    public $notes;
}
