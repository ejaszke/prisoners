<?php declare(strict_types=1);

namespace Prisoners\Application\Prisoner\Command\Handler;

use Broadway\CommandHandling\SimpleCommandHandler;
use Prisoners\Application\Prisoner\Command\AddPrisonerCommand;
use Prisoners\Domain\Model\Person;
use Prisoners\Domain\Model\Prisoner\Crime;
use Prisoners\Domain\Model\Prisoner\DurationOfStay;
use Prisoners\Domain\Model\Prisoner\Prisoner;
use Prisoners\Domain\Model\Prisoner\PrisonerRepository;

final class AddPrisonerHandler extends SimpleCommandHandler
{
    /**
     * @var PrisonerRepository
     */
    private $prisonerRepository;

    public function __construct(PrisonerRepository $prisonerRepository)
    {
        $this->prisonerRepository = $prisonerRepository;
    }

    public function handleAddPrisonerCommand(AddPrisonerCommand $addPrisonerCommand): void
    {
        $prisoner = Prisoner::create(
            $addPrisonerCommand->prisonerId,
            new Person($addPrisonerCommand->firstName, $addPrisonerCommand->lastName),
            new Crime($addPrisonerCommand->crime, new Crime\CrimeCategory($addPrisonerCommand->crimeCategory)),
            new DurationOfStay($addPrisonerCommand->startDate, $addPrisonerCommand->endDate),
            $addPrisonerCommand->notes
        );

        $this->prisonerRepository->save($prisoner);
    }
}
