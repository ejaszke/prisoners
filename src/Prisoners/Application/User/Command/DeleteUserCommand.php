<?php declare(strict_types=1);

namespace Prisoners\Application\User\Command;

use Prisoners\Domain\Model\User\UserId;

final class DeleteUserCommand
{
    /**
     * @var UserId
     */
    public $userId;

    /**
     * DeleteUserCommand constructor.
     */
    public function __construct(UserId $userId)
    {
        $this->userId = $userId;
    }
}
