<?php declare(strict_types=1);

namespace Prisoners\Application\Penitentiary\Command\Handler;

use Broadway\CommandHandling\SimpleCommandHandler;
use Prisoners\Application\Penitentiary\Command\ChangeCellCommand;
use Prisoners\Domain\Model\Penitentiary\PenitentiaryRepository;

final class ChangeCellHandler extends SimpleCommandHandler
{
    /**
     * @var PenitentiaryRepository
     */
    private $penitentiaryRepository;

    public function __construct(PenitentiaryRepository $penitentiaryRepository)
    {
        $this->penitentiaryRepository = $penitentiaryRepository;
    }

    public function handleChangeCellCommand(ChangeCellCommand $changeCellCommand): void
    {
        $penitentiary = $this->penitentiaryRepository->get($changeCellCommand->penitentiaryId);

        if ($penitentiary === null) {
            throw new \Exception('Penitentiary not found');
        }

        $penitentiary->changeCell($changeCellCommand->prisonerId, $changeCellCommand->block, $changeCellCommand->cellNumber);

        $this->penitentiaryRepository->save($penitentiary);
    }
}
