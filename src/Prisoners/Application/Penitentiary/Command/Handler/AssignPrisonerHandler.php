<?php declare(strict_types=1);

namespace Prisoners\Application\Penitentiary\Command\Handler;

use Broadway\CommandHandling\SimpleCommandHandler;
use Prisoners\Application\Penitentiary\Command\AssignPrisonerCommand;
use Prisoners\Domain\Model\Penitentiary\PenitentiaryRepository;

final class AssignPrisonerHandler extends SimpleCommandHandler
{
    /**
     * @var PenitentiaryRepository
     */
    private $penitentiaryRepository;

    public function __construct(PenitentiaryRepository $penitentiaryRepository)
    {
        $this->penitentiaryRepository = $penitentiaryRepository;
    }

    public function handleAssignPrisonerCommand(AssignPrisonerCommand $assignPrisonerCommand): void
    {
        $penitentiary = $this->penitentiaryRepository->get($assignPrisonerCommand->penitentiaryId);

        if ($penitentiary === null) {
            throw new \Exception('Penitentiary not found');
        }

        $penitentiary->assignPrisoner($assignPrisonerCommand->prisonerId, $assignPrisonerCommand->block, $assignPrisonerCommand->cellNumber);

        $this->penitentiaryRepository->save($penitentiary);
    }
}
