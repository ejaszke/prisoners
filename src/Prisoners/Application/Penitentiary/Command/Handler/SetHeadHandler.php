<?php declare(strict_types=1);

namespace Prisoners\Application\Penitentiary\Command\Handler;

use Broadway\CommandHandling\SimpleCommandHandler;
use Prisoners\Application\Penitentiary\Command\SetHeadCommand;
use Prisoners\Domain\Model\Penitentiary\PenitentiaryRepository;

final class SetHeadHandler extends SimpleCommandHandler
{
    /**
     * @var PenitentiaryRepository
     */
    private $penitentiaryRepository;

    public function __construct(PenitentiaryRepository $penitentiaryRepository)
    {
        $this->penitentiaryRepository = $penitentiaryRepository;
    }

    public function handleSetHeadCommand(SetHeadCommand $setHeadCommand): void
    {
        $penitentiary = $this->penitentiaryRepository->get($setHeadCommand->penitentiaryId);

        if ($penitentiary === null) {
            throw new \Exception('Penitentiary not found');
        }

        $penitentiary->setHead($setHeadCommand->userId);

        $this->penitentiaryRepository->save($penitentiary);
    }
}
