<?php declare(strict_types=1);

namespace Prisoners\Application\Penitentiary;

use Prisoners\Domain\ReadModel\Penitentiary\Penitentiary;
use Prisoners\Domain\ReadModel\Penitentiary\PenitentiaryPrisonersListRepository;
use Prisoners\Domain\ReadModel\User\UsersRepository;

final class PenitentiaryProvider
{
    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * @var PenitentiaryPrisonersListRepository
     */
    private $penitentiaryRepository;

    public function __construct(UsersRepository $usersRepository, PenitentiaryPrisonersListRepository $penitentiaryRepository)
    {
        $this->usersRepository = $usersRepository;
        $this->penitentiaryRepository = $penitentiaryRepository;
    }

    public function getForUser(string $username): array
    {
        $user = $this->usersRepository->findByUsername($username);

        if ($user === null) {
            throw new \Exception('User does not exit');
        }

        $penitentiaries = array_map(
            function (Penitentiary $penitentiary) {
                return $penitentiary->getId();
            },
            $this->penitentiaryRepository->fetchAll()
        );

        $roles = $user->getRoles();

        if ($roles && \in_array('ROLE_ADMIN', $roles, true)) {
            return $penitentiaries;
        }

        return [];
    }
}
